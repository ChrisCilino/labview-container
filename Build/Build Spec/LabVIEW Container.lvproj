<?xml version="1.0" encoding="UTF-8" standalone="no" ?><Project LVVersion="17008000" Type="Project">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="PetranWayTag:-:PetranWayAutoDocPrinterReportComboTag:-:PetranWay_Documentation Reports.lvlib:Project Report.lvclass::PetranWay_Documentation Reports.lvlib:BitBucket Wiki Markdown.lvclass" Type="Str">C:\PetranWay\Wikis\LabVIEW Container\Developer Resources\APIs\LabVIEW Container Project Documentation.md</Property>
	<Property Name="PetranWayTag:-:PetranWayAutoDocPrinterReportComboTag:-:PetranWay_Documentation Reports.lvlib:Project Report.lvclass::PetranWay_Documentation Reports.lvlib:Confluence.lvclass" Type="Str">215418676</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Build \ Install Stuff" Type="Folder">
			<Item Name="NIPKG" Type="Folder">
				<Item Name="NIPKG_LabVIEW Container_BuildSteps.lvlib" Type="Library" URL="../../Custom Build Steps/NIPKG_LV/NIPKG_LabVIEW Container_BuildSteps.lvlib"/>
				<Item Name="NIPKG_LabVIEW Container_InstallSteps.lvlib" Type="Library" URL="../../Custom Build Steps/NIPKG_LV/NIPKG_LabVIEW Container_InstallSteps.lvlib"/>
				<Item Name="VI Launcher.bat" Type="Document" URL="/&lt;userlib>/_PetranWay/Custom Install Step Launcher/Source/VI Launcher.bat"/>
			</Item>
			<Item Name="License.rtf" Type="Document" URL="../../License/License.rtf"/>
		</Item>
		<Item Name="Source" Type="Folder" URL="../../../Source">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Serializable.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializable/Serializable.lvclass"/>
				<Item Name="Serialization Common.lvlib" Type="Library" URL="/&lt;vilib>/NI/AQ Character Lineator/Serialization Common/Serialization Common.lvlib"/>
				<Item Name="Formatter.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Formatter/Formatter.lvclass"/>
				<Item Name="Serializer.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializer/Serializer.lvclass"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib>/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="/&lt;vilib>/Utility/LVClass/Qualified Name Array To Single String.vi"/>
				<Item Name="Class Retrieval.lvlib" Type="Library" URL="/&lt;vilib>/NI/Class Retrieval/Name to Path Conversion/Class Retrieval.lvlib"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib>/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib>/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Deserializer.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Deserializer/Deserializer.lvclass"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib>/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib>/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib>/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Serializable Variant Array.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializable Variant Array/Serializable Variant Array.lvclass"/>
				<Item Name="Serializable Variant.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializable Variant/Serializable Variant.lvclass"/>
				<Item Name="Serializable Digital Data.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializable Digital Data/Serializable Digital Data.lvclass"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Serializable Digital Waveform.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializable Digital Waveform/Serializable Digital Waveform.lvclass"/>
				<Item Name="Serializable Analog Waveform Array.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializable Analog Waveform Array/Serializable Analog Waveform Array.lvclass"/>
				<Item Name="Serializable Analog Waveform.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializable Analog Waveform/Serializable Analog Waveform.lvclass"/>
				<Item Name="Serializable Digital Data Array.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializable Digital Data Array/Serializable Digital Data Array.lvclass"/>
				<Item Name="Serializable Digital Waveform Array.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/Serializable Digital Waveform Array/Serializable Digital Waveform Array.lvclass"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib>/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="JSON Deserializer.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/JSON Deserializer/JSON Deserializer.lvclass"/>
				<Item Name="U64 Date-Time Record.ctl" Type="VI" URL="/&lt;vilib>/NI/AQ Character Lineator/U64 Date-Time Record.ctl"/>
				<Item Name="U64 Date-Time Record To Time Stamp.vi" Type="VI" URL="/&lt;vilib>/NI/AQ Character Lineator/U64 Date-Time Record To Time Stamp.vi"/>
				<Item Name="JSON Serializer.lvclass" Type="LVClass" URL="/&lt;vilib>/NI/AQ Character Lineator/JSON Serializer/JSON Serializer.lvclass"/>
				<Item Name="Time Stamp To U64 Date-Time Record.vi" Type="VI" URL="/&lt;vilib>/NI/AQ Character Lineator/Time Stamp To U64 Date-Time Record.vi"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib>/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib>/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib>/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib>/measure/NI_MABase.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib>/Analysis/NI_AALBase.lvlib"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib>/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib>/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib>/Utility/error.llb/DialogType.ctl"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib>/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Close File+.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Find First Error.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib>/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib>/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib>/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib>/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib>/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib>/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib>/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Write To Spreadsheet File.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib>/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib>/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="TRef TravTarget.ctl" Type="VI" URL="/&lt;vilib>/Utility/traverseref.llb/TRef TravTarget.ctl"/>
				<Item Name="VI Scripting - Traverse.lvlib" Type="Library" URL="/&lt;vilib>/Utility/traverseref.llb/VI Scripting - Traverse.lvlib"/>
				<Item Name="TRef Traverse.vi" Type="VI" URL="/&lt;vilib>/Utility/traverseref.llb/TRef Traverse.vi"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="/&lt;vilib>/xml/NI_XML.lvlib"/>
				<Item Name="JSONtext.lvlib" Type="Library" URL="/&lt;vilib>/JDP Science/JSONtext/JSONtext.lvlib"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib>/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="JDP Utility.lvlib" Type="Library" URL="/&lt;vilib>/JDP Science/JDP Science Common Utilities/JDP Utility.lvlib"/>
				<Item Name="JDP Timestamp.lvlib" Type="Library" URL="/&lt;vilib>/JDP Science/JDP Science Common Utilities/Timestamp/JDP Timestamp.lvlib"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib>/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib>/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="PetranWay_SrcDist BldSpec Utils.lvlib" Type="Library" URL="/&lt;userlib>/_PetranWay/SrcDist BldSpec Utils/Source/PetranWay_SrcDist BldSpec Utils.lvlib"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource>/lvanlys.dll"/>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="SrcDist" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{99DCA500-F991-405D-89C5-28D0F3B70CA8}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">SrcDist</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">/C/ProgramData/National Instruments/InstCache/14.0</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[3].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[4]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[4].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">5</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LabVIEW Container_BuildSteps.lvlib/Post-Build Action_SrcDist.vi</Property>
				<Property Name="Bld_preActionVIID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LabVIEW Container_BuildSteps.lvlib/Pre-Build Action_SrcDist.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{3CA134DB-F5EC-408A-8627-84EB5E9F7C49}</Property>
				<Property Name="Bld_userLogFile" Type="Path">/C/builds/LabVIEW Container/SrcDist/LabVIEW Container_SrcDist_log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">18</Property>
				<Property Name="Bld_version.major" Type="Int">2</Property>
				<Property Name="Bld_version.minor" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">CustomInstallSteps</Property>
				<Property Name="Destination[2].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/CustomInstallSteps</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[3].destName" Type="Str">License</Property>
				<Property Name="Destination[3].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/License</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[4].destName" Type="Str">NIPKGCustom Install Steps</Property>
				<Property Name="Destination[4].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/CustomInstallSteps/NIPKG</Property>
				<Property Name="Destination[4].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">5</Property>
				<Property Name="Source[0].itemID" Type="Str">{84CA199B-CEE2-4558-AB68-AA609D830096}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Source</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LabVIEW Container_BuildSteps.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">4</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LabVIEW Container_InstallSteps.lvlib</Property>
				<Property Name="Source[3].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Build \ Install Stuff/License.rtf</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[6].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">4</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG</Property>
				<Property Name="Source[6].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">7</Property>
			</Item>
			<Item Name="NIPKG" Type="{E661DAE2-7517-431F-AC41-30807A3BDA38}">
				<Property Name="NIPKG_license" Type="Ref">/My Computer/Build \ Install Stuff/License.rtf</Property>
				<Property Name="NIPKG_releaseNotes" Type="Str">


&lt;Upgrade Information&gt;

   Upgraded:    petranway-custominstallsteplauncher-nipkg-lv
           From:    1.1.0-27
              To:     1.1.0-28

&lt;/Upgrade Information&gt;</Property>
				<Property Name="PKG_actions.Count" Type="Int">3</Property>
				<Property Name="PKG_actions[0].Arguments" Type="Str">2017 32 "Run VI=Post-Install_NIPKG.vi"</Property>
				<Property Name="PKG_actions[0].NIPKG.HideConsole" Type="Bool">false</Property>
				<Property Name="PKG_actions[0].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[0].NIPKG.Schedule" Type="Str">post</Property>
				<Property Name="PKG_actions[0].NIPKG.Step" Type="Str">install</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Child" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Destination" Type="Str">{04DC6DD7-BD0C-45E3-A6F8-896248D25407}</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Source" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_actions[0].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[0].Type" Type="Str">NIPKG.Batch</Property>
				<Property Name="PKG_actions[1].Arguments" Type="Str">2017 32 "Run VI=Post-Install All_NIPKG.vi" "Mass Compile=False"</Property>
				<Property Name="PKG_actions[1].NIPKG.HideConsole" Type="Bool">false</Property>
				<Property Name="PKG_actions[1].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[1].NIPKG.Schedule" Type="Str">postall</Property>
				<Property Name="PKG_actions[1].NIPKG.Step" Type="Str">install</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Child" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Destination" Type="Str">{04DC6DD7-BD0C-45E3-A6F8-896248D25407}</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Source" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_actions[1].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[1].Type" Type="Str">NIPKG.Batch</Property>
				<Property Name="PKG_actions[2].Arguments" Type="Str">2017 32 "Run VI=Pre-Uninstall_NIPKG.vi"</Property>
				<Property Name="PKG_actions[2].NIPKG.HideConsole" Type="Bool">false</Property>
				<Property Name="PKG_actions[2].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[2].NIPKG.Schedule" Type="Str">pre</Property>
				<Property Name="PKG_actions[2].NIPKG.Step" Type="Str">uninstall</Property>
				<Property Name="PKG_actions[2].NIPKG.Target.Child" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="PKG_actions[2].NIPKG.Target.Destination" Type="Str">{04DC6DD7-BD0C-45E3-A6F8-896248D25407}</Property>
				<Property Name="PKG_actions[2].NIPKG.Target.Source" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_actions[2].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[2].Type" Type="Str">NIPKG.Batch</Property>
				<Property Name="PKG_autoIncrementBuild" Type="Bool">false</Property>
				<Property Name="PKG_autoSelectDeps" Type="Bool">false</Property>
				<Property Name="PKG_buildNumber" Type="Int">27</Property>
				<Property Name="PKG_buildSpecName" Type="Str">NIPKG</Property>
				<Property Name="PKG_dependencies.Count" Type="Int">2</Property>
				<Property Name="PKG_dependencies[0].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MaxVersion" Type="Str"/>
				<Property Name="PKG_dependencies[0].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MinVersion" Type="Str">0.9.3-47</Property>
				<Property Name="PKG_dependencies[0].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[0].NIPKG.DisplayName" Type="Str">NI AQ Character Lineator (LabVIEW 2017 32-bit)</Property>
				<Property Name="PKG_dependencies[0].Package.Name" Type="Str">ni-aq-character-lineator-lv2017-32-bit</Property>
				<Property Name="PKG_dependencies[0].Package.Section" Type="Str">Add-Ons</Property>
				<Property Name="PKG_dependencies[0].Package.Synopsis" Type="Str">A library for taking LabVIEW objects and reducing them to character sequences and then restoring the objects back later.</Property>
				<Property Name="PKG_dependencies[0].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_dependencies[1].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[1].MaxVersion" Type="Str"/>
				<Property Name="PKG_dependencies[1].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[1].MinVersion" Type="Str">1.1.0-28</Property>
				<Property Name="PKG_dependencies[1].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[1].NIPKG.DisplayName" Type="Str">Custom Install Step Launcher - NIPKG - LV</Property>
				<Property Name="PKG_dependencies[1].Package.Name" Type="Str">petranway-custominstallsteplauncher-nipkg-lv</Property>
				<Property Name="PKG_dependencies[1].Package.Section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_dependencies[1].Package.Synopsis" Type="Str">Launches the VI responsible for custom install \ unsinstall behavior.</Property>
				<Property Name="PKG_dependencies[1].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[1].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_description" Type="Str"/>
				<Property Name="PKG_destinations.Count" Type="Int">6</Property>
				<Property Name="PKG_destinations[0].ID" Type="Str">{04DC6DD7-BD0C-45E3-A6F8-896248D25407}</Property>
				<Property Name="PKG_destinations[0].Subdir.Directory" Type="Str">Source</Property>
				<Property Name="PKG_destinations[0].Subdir.Parent" Type="Str">{13043D46-7E1A-49FA-95F4-E8FD71E92C97}</Property>
				<Property Name="PKG_destinations[0].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[1].ID" Type="Str">{13043D46-7E1A-49FA-95F4-E8FD71E92C97}</Property>
				<Property Name="PKG_destinations[1].Subdir.Directory" Type="Str">LabVIEW Container</Property>
				<Property Name="PKG_destinations[1].Subdir.Parent" Type="Str">{FBB19A89-65D7-4F37-A1D6-6FC0CB612572}</Property>
				<Property Name="PKG_destinations[1].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[2].ID" Type="Str">{311382A9-E04F-4B3E-A1A7-D56B4E76ECD7}</Property>
				<Property Name="PKG_destinations[2].Subdir.Directory" Type="Str">LabVIEW 2017</Property>
				<Property Name="PKG_destinations[2].Subdir.Parent" Type="Str">{6EE9AACE-AA65-470E-A576-5DCCB3FAF13A}</Property>
				<Property Name="PKG_destinations[2].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[3].ID" Type="Str">{496F07A5-C12B-4121-A2F0-4BF800EED4DD}</Property>
				<Property Name="PKG_destinations[3].Subdir.Directory" Type="Str">user.lib</Property>
				<Property Name="PKG_destinations[3].Subdir.Parent" Type="Str">{311382A9-E04F-4B3E-A1A7-D56B4E76ECD7}</Property>
				<Property Name="PKG_destinations[3].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[4].ID" Type="Str">{6EE9AACE-AA65-470E-A576-5DCCB3FAF13A}</Property>
				<Property Name="PKG_destinations[4].Subdir.Directory" Type="Str">National Instruments</Property>
				<Property Name="PKG_destinations[4].Subdir.Parent" Type="Str">root_5</Property>
				<Property Name="PKG_destinations[4].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[5].ID" Type="Str">{FBB19A89-65D7-4F37-A1D6-6FC0CB612572}</Property>
				<Property Name="PKG_destinations[5].Subdir.Directory" Type="Str">_PetranWay</Property>
				<Property Name="PKG_destinations[5].Subdir.Parent" Type="Str">{496F07A5-C12B-4121-A2F0-4BF800EED4DD}</Property>
				<Property Name="PKG_destinations[5].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_displayName" Type="Str">LabVIEW Container - NIPKG</Property>
				<Property Name="PKG_displayVersion" Type="Str"/>
				<Property Name="PKG_homepage" Type="Str">https://bitbucket.org/ChrisCilino/labview-container</Property>
				<Property Name="PKG_lvrteTracking" Type="Bool">false</Property>
				<Property Name="PKG_maintainer" Type="Str">PetranWay &lt;christopher.cilino@petranway.com&gt;</Property>
				<Property Name="PKG_output" Type="Path">/C/builds/LabVIEW Container_LV/NIPKG</Property>
				<Property Name="PKG_packageName" Type="Str">petranway-labviewcontainer</Property>
				<Property Name="PKG_ProviderVersion" Type="Int">1810</Property>
				<Property Name="PKG_section" Type="Str">Add-Ons</Property>
				<Property Name="PKG_shortcuts.Count" Type="Int">0</Property>
				<Property Name="PKG_sources.Count" Type="Int">1</Property>
				<Property Name="PKG_sources[0].Destination" Type="Str">{04DC6DD7-BD0C-45E3-A6F8-896248D25407}</Property>
				<Property Name="PKG_sources[0].ID" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_sources[0].Type" Type="Str">Build</Property>
				<Property Name="PKG_sources[1].Destination" Type="Str">{6EE9AACE-AA65-470E-A576-5DCCB3FAF13A}</Property>
				<Property Name="PKG_sources[1].ID" Type="Ref"/>
				<Property Name="PKG_sources[1].Type" Type="Str">File</Property>
				<Property Name="PKG_synopsis" Type="Str">LabVIEW Container</Property>
				<Property Name="PKG_version" Type="Str">2.1.0</Property>
			</Item>
		</Item>
	</Item>
</Project>