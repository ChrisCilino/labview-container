# [Setup, Getting Started, And More Here](../../wiki/Home)

# About the Author(s), Contributors
|      |      |
|---------|---------|
|| [Chris Cilino](https://bit.ly/ChrisCilino_LinkedIn): I believe in good software engineering and healthy team builiding. I founded [PetranWay](https://www.petranway.com) to help software organizations grow thier software expertise, nurture team culture, and build infrastructure so that they produce quality products that meet business objectives. For more information check out [PetranWay](https://www.petranway.com).|


Please feel free to check out 

* [This Repository's Wiki](../../wiki/Home)
* [PetranWay](https://www.petranway.com)
* [My Online Presentations](http://bit.ly/ChrisCilino_Presentations)
* [My Free and Open Source Code](http://bit.ly/ChrisCilino_CSuite)
* [My LinkedIn Profile](http://bit.ly/ChrisCilino_LinkedIn)


# License
All software in this repository is licensed under the MIT license found in [..\Build\License\License.rtf](../master/Build/License/License.rtf). 